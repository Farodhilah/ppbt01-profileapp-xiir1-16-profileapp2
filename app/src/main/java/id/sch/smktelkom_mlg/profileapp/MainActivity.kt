package id.sch.smktelkom_mlg.profileapp

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val navController = Navigation.findNavController(this, R.id.navhost_fragment)
        bottom_nav?.let { bottomNavigationView ->
            NavigationUI.setupWithNavController(bottom_nav, navController)
        }
        navController.addOnNavigatedListener { controller, destination ->
            if (destination.id == R.id.familyFragment || destination.id == R.id.schoolFragment || destination.id == R.id.schoolFragment) {
                bottom_nav.let {
                    bottom_nav.visibility = View.GONE
                }
            } else {
                bottom_nav.visibility = View.VISIBLE
            }
        }
    }
}
